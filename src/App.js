import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Portfolio from './portfolio/portfolio.js'
import {
  Route,
  Link
} from 'react-router-dom'
const insideStyles = { padding: 15, position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%,-50%)'};
const mobileinsideStyles = { padding: 15, position: 'relative', top: '14%', left: '0%', right: '50%', transform: 'translate(-0%,-50%)'};


var styles = {
  tabs: {
    width: '100%',
  },
  side: {
    color: "black",
  },
  root: {
   flexGrow: 1,
 },
 flex: {
   flex: 1,
 },
 menuButton: {
   marginLeft: -12,
   marginRight: 20,
 },
};

var urls = [
  'https://www.linkedin.com/in/tyler-young-04a0b2a6/',
  'https://www.facebook.com/tyler.young.54922',
  'https://twitter.com/Tyler94556488',
  'https://www.instagram.com/tyler_young_k/?hl=en',
    'https://github.com/youngtk96',
  'https://stackoverflow.com/users/7600912/tyler-young',
];
class App extends Component {
  constructor(props) {
    super(props);
  this.state = {
      showNav: false
  }
  }

  render() {
    return (
      <div>
      <BrowserRouter>
  <Route path="/portfolio" component={Portfolio}/>
</BrowserRouter>

</div>
    );
  }
}

export default App;
