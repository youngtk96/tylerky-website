import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { SocialIcons } from 'react-social-icons';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import FlatButton from 'material-ui/FlatButton';
import { Image } from 'semantic-ui-react'
import pic from './img/download.png'
import back from './img/back.jpg'
import reactjs from './img/reactjs.png'
import reactnative from './img/reactnative.png'
import javascript from './img/javascript.png'
import html from './img/html.png'
import php from './img/php.png'
import nodejs from './img/nodejs.png'
import css from './img/css.png'
import python from './img/python.png'
import npm from './img/npm.png'
import mysql from './img/mysql.png'
import mssql from './img/mssql.png'
import mongodb from './img/mongodb.png'
import fm from './img/fm.png'
import apache from './img/apache.png'
import csharp from './img/csharp.png'
import hyper from './img/hyper.png'
import nginx from './img/nginx.png'
import iis from './img/iis.png'
import atom from './img/atom.png'
import graphql from './img/graphql.png'
import expo from './img/expo.png'
import pc from './img/pc.png'
import electron from './img/electron.png'
import loopback from './img/loopback.png'
import { Progress, Segment } from 'semantic-ui-react'
import AutoScale from 'react-auto-scale';
import { Parallax, Background } from 'react-parallax';
import MediaQuery from 'react-responsive';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
const insideStyles = { padding: 15, position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%,-50%)'};
const mobileinsideStyles = { padding: 15, position: 'relative', top: '14%', left: '0%', right: '50%', transform: 'translate(-0%,-50%)'};


var styles = {
  tabs: {
    width: '100%',
  },
  side: {
    color: "#2a2a2a",
  },
  root: {
   flexGrow: 1,
 },
 flex: {
   flex: 1,
 },
 menuButton: {
   marginLeft: -12,
   marginRight: 20,
 },
};

var urls = [
  'https://www.linkedin.com/in/tyler-young-04a0b2a6/',
  'https://www.facebook.com/tyler.young.54922',
  'https://twitter.com/Tyler94556488',
  'https://www.instagram.com/tyler_young_k/?hl=en',
    'https://github.com/youngtk96',
  'https://stackoverflow.com/users/7600912/tyler-young',
];
class Portfolio extends Component {
  constructor(props) {
    super(props);
  this.state = {
      showNav: false
  }
  }

  render() {
    return (


      <div>
       <MediaQuery query="(min-device-width: 1224px)">
      <div style={{backgroundColor: "#2a2a2a"}}>
      <AppBar style={{backgroundColor: "#20c17a"}}position="fixed">
       <Toolbar>
         <IconButton className={styles.menuButton} color="inherit" aria-label="Menu">
           <MenuIcon />
         </IconButton>
         <Typography variant="title" color="inherit" className={styles.flex}>
           Tyler Young
         </Typography>
         <Button style={{padding: 10}} color="inherit">Portfolio</Button>
         <Button style={{padding: 10}} color="inherit">Projects/Applications</Button>
          <Button style={{padding: 10}} color="inherit">Blog</Button>
       </Toolbar>
     </AppBar>
   <Parallax style={{ height: '500px', overflow: 'wrap' }} bgImage={back}
  strength={500}>

  <div style={{height: 500, width: 10}}>

   <div style={insideStyles}>

  <SocialIcons style={{textAlign: 'center'}} urls={urls} />
  <br></br>
   <Image src={pic} size='small' circular centered />
   <h1 style={{color: "white", fontSize: 50, textAlign: 'center'}}>
   Tyler Young
   </h1>
   <p style={{color: "white", fontStyle: 'italic', fontSize: 50, textAlign: 'center'}}>
   Full Stack Software Developer
   </p>
   <p style={{color: "white",  fontWeight: 'bold',fontSize: 15, textAlign: 'center'}}>
  I am a passionate full stack software developer. I love being able to develop every aspect of software. whether its UI/UX, APIs, Database infrastructure, or setting up and maintaining the servers. I believe that software, if done correctly, can help a business and its customers grow and prosper together.
   </p>

   </div>


  </div>

  </Parallax>
  <br></br>
  <div style={{borderColor: 'white'}}>
  <h1 style={{color: "white",textAlign: 'center',  borderColor: 'white', borderBottomWidth: 100,}}>
  Programming Langauges/Technologies
  </h1>
  </div>
  <div class="skills">
  <div class="imggrey">
  <a onClick={console.log("clicked")}>
  <Image  style={{width: 60}}src={javascript} size='small' centered />
  </a>
  </div>
  <p style={{color: "white",textAlign: 'center'}}> JavaScript</p>
  <Progress percent={100} inverted indicating active size='small'>

   </Progress>
   </div>

   <div class="skills">
   <div class="imggrey">
   <Image style={{width: 60}}src={html} size='small' centered />
   </div>
   <p style={{color: "white",textAlign: 'center'}}>HTML</p>
   <Progress percent={100} inverted indicating active size='small'>

      </Progress>

      </div>
      <div class="skills">
      <div class="imggrey">
      <Image style={{width: 60}}src={css} size='small' centered />
      </div>
      <p style={{color: "white",textAlign: 'center'}}>CSS</p>
      <Progress percent={100} inverted indicating active size='small'>

         </Progress>
         </div>

         <div class="skills">
         <div class="imggrey">
         <Image style={{width: 120}}  src={nodejs} size='small' centered />
         </div>
         <p style={{color: "white",textAlign: 'center'}}>node.js</p>
         <Progress percent={100} inverted indicating active size='small'>

            </Progress>
            </div>
   <div class="skills">
   <div class="imggrey">
   <Image src={reactjs} size='small' centered />
   </div>
   <p style={{color: "white",textAlign: 'center'}}> React.JS</p>
   <Progress percent={100} inverted indicating active size='small'>

      </Progress>
      </div>

      <div class="skills">
      <div class="imggrey">
      <Image style={{width: 250}} src={reactnative} size='small' centered />
      </div>
      <p style={{color: "white",textAlign: 'center'}}> React Native</p>
      <Progress percent={100} inverted indicating active size='small'>

         </Progress>
         </div>

         <div class="skills">
         <div class="imggrey">
         <Image src={php} size='small' centered />
         </div>
         <p style={{color: "white",textAlign: 'center'}}>PHP</p>
         <Progress percent={100} inverted indicating active size='small'>

            </Progress>
            </div>

            <div class="skills">
            <div class="imggrey">
            <Image src={npm} size='small' centered />
            </div>
            <p style={{color: "white",textAlign: 'center'}}>NPM</p>
            <Progress percent={100} inverted indicating active size='small'>

               </Progress>
               </div>

            <div class="skills">
            <div class="imggrey">
            <Image src={python} size='small' centered />
            </div>
            <p style={{color: "white",textAlign: 'center'}}>Python</p>
            <Progress percent={60} inverted indicating active size='small'>

               </Progress>
               </div>

               <div class="skills">
               <div class="imggrey">
               <Image style={{width: 90}} src={csharp} size='small' centered />
               </div>
               <p style={{color: "white",textAlign: 'center'}}>C#</p>
               <Progress percent={40} inverted indicating active size='small'>

                  </Progress>
                  </div>
  <br></br>
  <h1 style={{color: "white",textAlign: 'center',  borderColor: 'white', borderBottomWidth: 100,}}>
  Databases
  </h1>
  <div class="skills">
  <div class="imggrey">
  <Image src={mysql} size='small' centered />
  </div>
  <p style={{color: "white",textAlign: 'center'}}>MySQL</p>
  <Progress percent={100} inverted indicating active size='small'>

   </Progress>
   </div>

   <div class="skills">
   <div class="imggrey">
   <Image src={mssql} size='small' centered />
   </div>
   <p style={{color: "white",textAlign: 'center'}}>Microsoft SQL</p>
   <Progress percent={100} inverted indicating active size='small'>

      </Progress>
      </div>

      <div class="skills">
      <div class="imggrey">
      <Image style={{width: 90}} src={fm} size='small' centered />
      </div>
      <p style={{color: "white",textAlign: 'center'}}>FileMaker</p>
      <Progress percent={90} inverted indicating active size='small'>

         </Progress>
         </div>



         <div class="skills">
         <div class="imggrey">
         <Image src={mongodb} size='small' centered />
         </div>
         <p style={{color: "white",textAlign: 'center'}}>Mongodb</p>
         <Progress percent={80} inverted indicating active size='small'>

            </Progress>
            </div>


                     <div class="skills">
                     <div class="imggrey">
                     <Image style={{width: 90}} src={hyper} size='small' centered />
                     </div>
                     <p style={{color: "white",textAlign: 'center'}}>Hyper Ledger</p>
                     <Progress percent={60} inverted indicating active size='small'>

                        </Progress>
                        </div>
  <br></br>
  <h1 style={{color: "white",textAlign: 'center',  borderColor: 'white', borderBottomWidth: 100,}}>
  Web Servers
  </h1>
  <div class="skills">
  <div class="imggrey">
  <Image src={nginx} size='small' centered />
  </div>
  <p style={{color: "white",textAlign: 'center'}}>NGINX</p>
  <Progress percent={100} inverted indicating active size='small'>

   </Progress>
   </div>
   <div class="skills">
   <div class="imggrey">
   <Image style={{width: 120}}  src={nodejs} size='small' centered />
   </div>
   <p style={{color: "white",textAlign: 'center'}}>node.js</p>
   <Progress percent={100} inverted indicating active size='small'>

      </Progress>
      </div>

      <div class="skills">
      <div class="imggrey">
      <Image style={{width: 120}}  src={apache} size='small' centered />
      </div>
      <p style={{color: "white",textAlign: 'center'}}>Apache</p>
      <Progress percent={100} inverted indicating active size='small'>

         </Progress>
         </div>


         <div class="skills">
         <div class="imggrey">
         <Image style={{width: 120}}  src={iis} size='small' centered />
         </div>
         <p style={{color: "white",textAlign: 'center'}}>IIS</p>
         <Progress percent={100} inverted indicating active size='small'>

            </Progress>
            </div>


            <br></br>
            <h1 style={{color: "white",textAlign: 'center',  borderColor: 'white', borderBottomWidth: 100,}}>
            Notable Tools/Packages
            </h1>

            <div class="skills">
            <div class="imggrey">
            <Image style={{width: 120}} src={atom} size='small' centered />
            </div>
            <p style={{color: "white",textAlign: 'center'}}>Atom (prefered IDE)</p>
            <Progress percent={100} inverted indicating active size='small'>

               </Progress>
               </div>

               <div class="skills">
               <div class="imggrey">
               <Image style={{width: 120}} src={pc} size='small' centered />
               </div>
               <p style={{color: "white",textAlign: 'center'}}>PyCharm</p>
               <Progress percent={100} inverted indicating active size='small'>

                  </Progress>
                  </div>
                  <div class="skills">
                  <div class="imggrey">
                  <Image style={{width: 120}} src={loopback} size='small' centered />
                  </div>
                  <p style={{color: "white",textAlign: 'center'}}>Loopback</p>
                  <Progress percent={100} inverted indicating active size='small'>

                     </Progress>
                     </div>


               <div class="skills">
               <div class="imggrey">
               <Image style={{width: 120}} src={graphql} size='small' centered />
               </div>
               <p style={{color: "white",textAlign: 'center'}}>GraphQL</p>
               <Progress percent={100} inverted indicating active size='small'>

                  </Progress>
                  </div>

                  <div class="skills">
                  <div class="imggrey">
                  <Image style={{width: 120}} src={expo} size='small' centered />
                  </div>
                  <p style={{color: "white",textAlign: 'center'}}>EXPO</p>
                  <Progress percent={100} inverted indicating active size='small'>

                     </Progress>
                     </div>


                     <div class="skills">
                     <div class="imggrey">
                     <Image style={{width: 120}} src={electron} size='small' centered />
                     </div>
                     <p style={{color: "white",textAlign: 'center'}}>Electron</p>
                     <Progress percent={100} inverted indicating active size='small'>

                        </Progress>
                        </div>

      </div>
  </MediaQuery>
  {/*for mobile*/}
   <MediaQuery query="(max-device-width: 1224px)">

  <div style={{backgroundColor: "#2a2a2a"}}>
  <AppBar style={{backgroundColor: "#2a2a2a"}} position="static">
   <Toolbar>
     <IconButton className={styles.menuButton} color="inherit" aria-label="Menu">
       <MenuIcon />
     </IconButton>
     <Typography variant="title" color="inherit" className={styles.flex}>
       Tyler Young
     </Typography>
     <Button color="inherit">Portfolio</Button>
   </Toolbar>
 </AppBar>
<Parallax style={{ height: '500px', overflow: 'wrap' }} bgImage={back}
strength={500}>


  <div style={{height: 500, width: "100%"}}>

    <div style={mobileinsideStyles}>
    <br></br>
   <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
                    <br></br>
                        <br></br>
                            <br></br>
                            <br></br>
                                <br></br>
                                    <br></br>
    <SocialIcons style={{textAlign: 'center'}} urls={urls} />
    <br></br>
    <Image src={pic} style={{height: 100}} circular centered />
    <h1 style={{color: "white", fontSize: 30, textAlign: 'center', width: "100%"}}>
    Tyler Young
    </h1>
    <p style={{color: "white", fontStyle: 'italic', fontSize: 20, textAlign: 'center'}}>
    Full Stack Software Developer
    </p>
    <p style={{color: "white",  fontWeight: 'bold',fontSize: 14, textAlign: 'center'}}>
   I am a passionate full stack software developer. I love being able to develop every aspect of software. whether its UI/UX, APIs, Database infrastructure, or setting up and maintaining the servers. I believe that software, if done correctly, can help a business and its customers grow and prosper together.
    </p>
    </div>

  </div>

</Parallax>
<br></br>
<div style={{borderColor: 'white'}}>
<h1 style={{color: "white",textAlign: 'center', fontSize: "20px",  borderColor: 'white', borderBottomWidth: 100,}}>
Programming Langauges/Technologies
</h1>
<br></br>
</div>
<div class="skillsM">
<div class="imggrey">
<a onClick={console.log("clicked")}>
<Image  style={{width: 60}}src={javascript} size='small' centered />
</a>
</div>
<p style={{color: "white",textAlign: 'center'}}> JavaScript</p>
<Progress percent={100} inverted indicating active size='small'>

 </Progress>
 </div>

 <div class="skillsM">
 <div class="imggrey">
 <Image style={{width: 60}}src={html} size='small' centered />
 </div>
 <p style={{color: "white",textAlign: 'center'}}>HTML</p>
 <Progress percent={100} inverted indicating active size='small'>

    </Progress>

    </div>
    <div class="skillsM">
    <div class="imggrey">
    <Image style={{width: 60}}src={css} size='small' centered />
    </div>
    <p style={{color: "white",textAlign: 'center'}}>CSS</p>
    <Progress percent={100} inverted indicating active size='small'>

       </Progress>
       </div>

       <div class="skillsM">
       <div class="imggrey">
       <Image style={{width: 120}}  src={nodejs} size='small' centered />
       </div>
       <p style={{color: "white",textAlign: 'center'}}>node.js</p>
       <Progress percent={100} inverted indicating active size='small'>

          </Progress>
          </div>
 <div class="skillsM">
 <div class="imggrey">
 <Image src={reactjs} size='small' centered />
 </div>
 <p style={{color: "white",textAlign: 'center'}}> React.JS</p>
 <Progress percent={100} inverted indicating active size='small'>

    </Progress>
    </div>

    <div class="skillsM">
    <div class="imggrey">
    <Image style={{width: 250}} src={reactnative} size='small' centered />
    </div>
    <p style={{color: "white",textAlign: 'center'}}> React Native</p>
    <Progress percent={100} inverted indicating active size='small'>

       </Progress>
       </div>

       <div class="skillsM">
       <div class="imggrey">
       <Image src={php} size='small' centered />
       </div>
       <p style={{color: "white",textAlign: 'center'}}>PHP</p>
       <Progress percent={100} inverted indicating active size='small'>

          </Progress>
          </div>

          <div class="skillsM">
          <div class="imggrey">
          <Image src={npm} size='small' centered />
          </div>
          <p style={{color: "white",textAlign: 'center'}}>NPM</p>
          <Progress percent={100} inverted indicating active size='small'>

             </Progress>
             </div>

          <div class="skillsM">
          <div class="imggrey">
          <Image src={python} size='small' centered />
          </div>
          <p style={{color: "white",textAlign: 'center'}}>Python</p>
          <Progress percent={60} inverted indicating active size='small'>

             </Progress>
             </div>

             <div class="skillsM">
             <div class="imggrey">
             <Image style={{width: 90}} src={csharp} size='small' centered />
             </div>
             <p style={{color: "white",textAlign: 'center'}}>C#</p>
             <Progress percent={40} inverted indicating active size='small'>

                </Progress>
                </div>
<br></br>
<h1 style={{color: "white",textAlign: 'center',  borderColor: 'white', borderBottomWidth: 100,}}>
Databases
</h1>
<div class="skillsM">
<div class="imggrey">
<Image src={mysql} size='small' centered />
</div>
<p style={{color: "white",textAlign: 'center'}}>MySQL</p>
<Progress percent={100} inverted indicating active size='small'>

 </Progress>
 </div>

 <div class="skillsM">
 <div class="imggrey">
 <Image src={mssql} size='small' centered />
 </div>
 <p style={{color: "white",textAlign: 'center'}}>Microsoft SQL</p>
 <Progress percent={100} inverted indicating active size='small'>

    </Progress>
    </div>

    <div class="skillsM">
    <div class="imggrey">
    <Image style={{width: 90}} src={fm} size='small' centered />
    </div>
    <p style={{color: "white",textAlign: 'center'}}>FileMaker</p>
    <Progress percent={90} inverted indicating active size='small'>

       </Progress>
       </div>



       <div class="skillsM">
       <div class="imggrey">
       <Image src={mongodb} size='small' centered />
       </div>
       <p style={{color: "white",textAlign: 'center'}}>Mongodb</p>
       <Progress percent={80} inverted indicating active size='small'>

          </Progress>
          </div>


                   <div class="skillsM">
                   <div class="imggrey">
                   <Image style={{width: 90}} src={hyper} size='small' centered />
                   </div>
                   <p style={{color: "white",textAlign: 'center'}}>Hyper Ledger</p>
                   <Progress percent={60} inverted indicating active size='small'>

                      </Progress>
                      </div>
<br></br>
<h1 style={{color: "white",textAlign: 'center',  borderColor: 'white', borderBottomWidth: 100,}}>
Web Servers
</h1>
<div class="skillsM">
<div class="imggrey">
<Image src={nginx} size='small' centered />
</div>
<p style={{color: "white",textAlign: 'center'}}>NGINX</p>
<Progress percent={100} inverted indicating active size='small'>

 </Progress>
 </div>
 <div class="skillsM">
 <div class="imggrey">
 <Image style={{width: 120}}  src={nodejs} size='small' centered />
 </div>
 <p style={{color: "white",textAlign: 'center'}}>node.js</p>
 <Progress percent={100} inverted indicating active size='small'>

    </Progress>
    </div>

    <div class="skillsM">
    <div class="imggrey">
    <Image style={{width: 120}}  src={apache} size='small' centered />
    </div>
    <p style={{color: "white",textAlign: 'center'}}>Apache</p>
    <Progress percent={100} inverted indicating active size='small'>

       </Progress>
       </div>


       <div class="skillsM">
       <div class="imggrey">
       <Image style={{width: 120}}  src={iis} size='small' centered />
       </div>
       <p style={{color: "white",textAlign: 'center'}}>IIS</p>
       <Progress percent={100} inverted indicating active size='small'>

          </Progress>
          </div>


          <br></br>
          <h1 style={{color: "white",textAlign: 'center',  borderColor: 'white', borderBottomWidth: 100,}}>
          Notable Tools/Packages
          </h1>

          <div class="skillsM">
          <div class="imggrey">
          <Image style={{width: 120}} src={atom} size='small' centered />
          </div>
          <p style={{color: "white",textAlign: 'center'}}>Atom (prefered IDE)</p>
          <Progress percent={100} inverted indicating active size='small'>

             </Progress>
             </div>

             <div class="skillsM">
             <div class="imggrey">
             <Image style={{width: 120}} src={pc} size='small' centered />
             </div>
             <p style={{color: "white",textAlign: 'center'}}>PyCharm</p>
             <Progress percent={100} inverted indicating active size='small'>

                </Progress>
                </div>
                <div class="skillsM">
                <div class="imggrey">
                <Image style={{width: 120}} src={loopback} size='small' centered />
                </div>
                <p style={{color: "white",textAlign: 'center'}}>Loopback</p>
                <Progress percent={100} inverted indicating active size='small'>

                   </Progress>
                   </div>


             <div class="skillsM">
             <div class="imggrey">
             <Image style={{width: 120}} src={graphql} size='small' centered />
             </div>
             <p style={{color: "white",textAlign: 'center'}}>GraphQL</p>
             <Progress percent={100} inverted indicating active size='small'>

                </Progress>
                </div>

                <div class="skillsM">
                <div class="imggrey">
                <Image style={{width: 120}} src={expo} size='small' centered />
                </div>
                <p style={{color: "white",textAlign: 'center'}}>EXPO</p>
                <Progress percent={100} inverted indicating active size='small'>

                   </Progress>
                   </div>


                   <div class="skillsM">
                   <div class="imggrey">
                   <Image style={{width: 120}} src={electron} size='small' centered />
                   </div>
                   <p style={{color: "white",textAlign: 'center'}}>Electron</p>
                   <Progress percent={100} inverted indicating active size='small'>

                      </Progress>
                      </div>
                      </div>

</MediaQuery>

</div>
    );
  }
}

export default Portfolio;
